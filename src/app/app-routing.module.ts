
import { ContactComponent } from './contact/contact.component';
import { EventsComponent } from './events/events.component';
import { AboutComponent } from './about/about.component';
import { ServicesComponent } from './services/services.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { HomeComponent } from './home/home.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { from } from 'rxjs';

const routes: Routes = [
  {path: '',component:HomeComponent},
  {path: 'login',component:LoginComponent},
  {path:'register',component:RegisterComponent},
  {path: 'services',component:ServicesComponent},
  {path:'about',component:AboutComponent},
  {path:'home',component:HomeComponent},
  {path:'events',component:EventsComponent},
  {path:'contact',component:ContactComponent},
  {path:'**',component:NotFoundComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
