import { AuthService } from './services/auth.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './home/home.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ServicesComponent } from './services/services.component';
import { AboutComponent } from './about/about.component';
import { EventsComponent } from './events/events.component';
import { ContactComponent } from './contact/contact.component';
import { SharedComponent } from './shared/shared.component';
import { FirstComponent } from './shared/first/first.component';
import * as firebase from 'firebase/app';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StorageBucket, AngularFireStorageModule } from '@angular/fire/storage';
import { ContactService } from './services/contact.service';
import { AngularFireModule } from '@angular/fire';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    NotFoundComponent,
    LoginComponent,
    RegisterComponent,
    ServicesComponent,
    AboutComponent,
    EventsComponent,
    ContactComponent,
    SharedComponent,
    FirstComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFireStorageModule,
  ],
  providers: [
    AuthService,
    { provide: StorageBucket, useValue: 'gs://yoga-b7275.appspot.com/uploads' },
    ContactService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor() {
    var config = {
      apiKey: "AIzaSyDtcye9bFGCpxw9KpGwgS0v_OSzxxjnMWg",
      authDomain: "yoga-b7275.firebaseapp.com",
      databaseURL: "https://yoga-b7275.firebaseio.com",
      projectId: "yoga-b7275",
      storageBucket: "gs://yoga-b7275.appspot.com/",
      messagingSenderId: "976302499876"
    };
    firebase.initializeApp(config);
  }
}
