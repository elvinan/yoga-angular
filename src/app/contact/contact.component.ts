import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ContactService } from '../services/contact.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  contactForm: FormGroup;
  file: File;
  constructor(private fb: FormBuilder, private contactService: ContactService) { }

  ngOnInit() {
    this.contactForm = this.fb.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', Validators.required],
      phoneNumber: ['', Validators.required],
      message: ['', Validators.required],
      image: ['']
    })
  }

  async onSend() {
    console.log(this.contactForm.value);

    if (this.file) {
      let image = await this.contactService.uploadFile(this.file)
      console.log(image);

    }


  }

  imageUpload(event) {
    let reader = new FileReader();
    //get the selected file from event
    this.file = event.target.files[0];

  }

}
