import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  first=[
    {url: "assets/images/s1.jpg",title: "Flexibility"},
    {url: "assets/images/s2.jpg",title: "Health"},
    {url: "assets/images/s3.jpg",title: "Relax"},
    {url: "assets/images/s4.jpg",title: "Support"},
    {url: "assets/images/s5.jpg",title: "Health"},
    {url: "assets/images/s6.jpg",title: "Spirituality"},
  ]
  constructor() { }

  ngOnInit() {
  }

}
