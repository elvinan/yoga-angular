import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  formLogin:FormGroup; //create form
  constructor(private authService: AuthService, private fb: FormBuilder) { }

  ngOnInit() {
    //Init & validate form
    this.formLogin = this.fb.group({
      email: ['', [Validators.required,Validators.email]],
      password: ['', [Validators.required,Validators.minLength(6)]]
    });
  }
  
  onLogin() {
    //call service func
    this.authService.onLogin(this.formLogin.value);
  }

}
