import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { AuthService } from "./../services/auth.service";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-register",
  templateUrl: "./register.component.html",
  styleUrls: ["./register.component.css"]
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;

  constructor(private auth: AuthService, private fb: FormBuilder) {}

  ngOnInit() {
    this.registerForm = this.fb.group({
      email: ["", Validators.required],
      password: ["", Validators.required],
      repeatPassword: ["", Validators.required]
    });
  }

  onRegister() {
    const { email, password, repeatPassword } = this.registerForm.value;

    if (password == repeatPassword) {
      this.auth
        .onRegister({ email, password })
        .then(res => {
          console.log(res);
        })
        .catch(err => {
          console.log(err);
        });
    } else {
      alert("Mat khau *eo trung!");
    }
  }
}
