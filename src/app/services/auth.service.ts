
import { Injectable } from '@angular/core';
import  * as firebase from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }
  onLogin(user){
    firebase.auth().signInWithEmailAndPassword(user.email,user.password)
    .then(res=>{
      console.log(res);
    }).catch(err=>{
      console.log(err);
    })
  }
  onRegister(user){
    return firebase.auth().createUserWithEmailAndPassword(user.email,user.password)
  }
}
