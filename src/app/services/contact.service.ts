import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import { AngularFireStorage } from '@angular/fire/storage'
@Injectable({
  providedIn: 'root'
})
export class ContactService {
  storage;
  constructor() {
  }

  sendMessage(data) {
    return firebase.firestore().doc(Math.random().toString())
  }

  uploadFile(file: File) {
    console.log(file);

    return firebase.storage().ref().child("uploads/" + file.name).put(file);
  }
}
